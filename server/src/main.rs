extern crate tokio;
extern crate warp;

#[tokio::main]
async fn main() {
    let default_route = warp::fs::dir("public");
    warp::serve(default_route).run(([127, 0, 0, 1], 3030)).await;
}
