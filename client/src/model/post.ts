import firebase from "firebase";
import { Option } from "src/lib/option";

export type Post = {
    category: string[];
    periodBegin: firebase.firestore.Timestamp;
    periodEnd: firebase.firestore.Timestamp;
    title: string;
    content: string;
    img: Option<string>;
};
