export interface Option<T> {
    or(value: T): T;
    map<U>(f: (x: T) => U): Option<U>;
    asyncMap<U>(f: (x: T) => Promise<U>): Promise<Option<U>>;
    unwrap(): T | null;
}

export class Some<T> implements Option<T> {
    constructor(public payload: T) {}

    or(_: T): T {
        return this.payload;
    }

    map<U>(f: (x: T) => U): Option<U> {
        return new Some(f(this.payload));
    }

    asyncMap<U>(f: (x: T) => Promise<U>): Promise<Option<U>> {
        return f(this.payload).then((x) => new Some(x));
    }

    unwrap(): T | null {
        return this.payload;
    }
}

export class None implements Option<never> {
    constructor() {}

    or<T>(value: T): T {
        return value;
    }

    map<U>(f: (x: never) => U): Option<U> {
        return new None();
    }

    asyncMap<U>(f: (x: never) => Promise<U>): Promise<Option<U>> {
        return Promise.resolve(new None());
    }

    unwrap(): never | null {
        return null;
    }
}
