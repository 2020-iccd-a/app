import styled from "styled-components";
import React from "react";
import { color } from "src/color";

const Base = styled.div`
    display: grid;
    grid-template-rows: max-content 1fr;
    overflow-y: hidden;
`;

export const Heading = styled.div`
    border-bottom: 0.1em solid ${color.gray[5]};
    padding: 0.65em;
    display: grid;
    grid-template-columns: 1fr max-content;

    & h1 {
        margin: 0;
    }
`;

export const CardList = styled.div`
    padding: 0.65em;
    overflow-y: scroll;
    display: grid;
    grid-template-columns: 1fr;
    grid-auto-rows: max-content;
    row-gap: 0.65em;
`;

type Props = {};

export const Page = (props: React.PropsWithChildren<Props>) => {
    return <Base>{props.children}</Base>;
};
