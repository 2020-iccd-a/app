import styled from "styled-components";
import React, { useState } from "react";
import { color } from "src/color";
import firebase from "firebase/app";
import "firebase/firestore";
import { SideMenu } from "src/component/organism/side-menu";
import { Post } from "src/model/post";
import { Home } from "src/component/page/home";
import { Category } from "src/component/page/category";
import { Option, Some, None } from "src/lib/option";
import { Env } from "src/model/env";

type Props = {
    fs: firebase.firestore.Firestore;
};

const Base = styled.div`
    position: fixed;
    width: 100vw;
    height: 100vh;
    top: 0;
    left: 0;

    display: grid;
    grid-template-columns: max-content 1fr;
    grid-template-rows: 1fr;

    background-color: ${color.gray[0]};
`;

const Survey = styled.div`
    position: fixed;
    bottom: 5rem;
    right: 5rem;
    padding: 0.65rem;
    background-color: ${color.blue[3]};
    border-radius: 0.35rem;
    z-index: 1;
`;

const loadPost = async (
    fs: firebase.firestore.Firestore,
    setPosts: React.Dispatch<React.SetStateAction<Option<Post[]>>>
) => {
    const collection = fs.collection("posts");
    collection.onSnapshot(async () => {
        const collection = fs.collection("posts");
        const raw_posts = await collection.orderBy("periodBegin").get();
        const posts: Post[] = [];
        raw_posts.forEach((doc) => {
            // TODO: 型チェック
            const data = doc.data();
            const category = data.category;
            const periodBegin = data.periodBegin;
            const periodEnd = data.periodEnd;
            const title = data.title;
            const content = data.content;
            const img = (() => {
                if (data.img) {
                    const imgType = data.img.type;
                    const imgData = (data.img
                        .data as firebase.firestore.Blob).toUint8Array();
                    const imgBlob = new Blob([imgData], { type: imgType });
                    return new Some(URL.createObjectURL(imgBlob));
                } else {
                    return new None();
                }
            })();
            posts.push({
                category,
                periodBegin,
                periodEnd,
                title,
                content,
                img,
            });
        });
        setPosts(new Some(posts.reverse()));
    });
};

export const App = (props: Props) => {
    const [posts, setPosts] = useState<Option<Post[]>>(new None());
    const [selectedCategory, setSetectedCategory] = useState<string>("");

    const [env, setEnv] = useState<Env>("Student");

    if (posts instanceof None) {
        loadPost(props.fs, setPosts);
        setPosts(new Some([]));
    }

    return (
        <Base>
            <Survey>
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSdlfvE-P4RJv7IPqqph7l7N_VyKINy_l_HxmnNbLLG0RTWW0g/viewform?usp=sf_link">
                    ご意見・ご要望等
                </a>
            </Survey>
            <SideMenu
                posts={posts.or([])}
                selected={selectedCategory}
                setSelected={setSetectedCategory}
            />
            {selectedCategory == "" ? (
                <Home
                    posts={posts.or([])}
                    env={env}
                    setEnv={(env) => setEnv(env)}
                />
            ) : (
                    <Category
                        category={selectedCategory}
                        posts={posts.or([])}
                        env={env}
                        fs={props.fs}
                    />
                )}
        </Base>
    );
};
