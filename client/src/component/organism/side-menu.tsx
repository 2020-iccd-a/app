import styled, { css } from "styled-components";
import { color } from "src/color";
import { MenuItem } from "src/component/atom/menu-item";
import { MenuButton } from "src/component/atom/menu-button";
import React, { Fragment, useState } from "react";
import { Modal } from "src/component/molecule/modal";
import { Input } from "src/component/atom/input";
import { KeyValue } from "src/component/atom/key-value";
import { Btn } from "src/component/atom/btn";
import firebase from "firebase/app";
import "firebase/firestore";
import { Post } from "src/model/post";

const Base = styled.div`
    min-width: 20rem;
    max-width: 10vw;

    box-shadow: 0 0 0.1em 0.07em ${color.gray[9]};
    background-color: ${color.gray[1]};
`;

const ModalBody = styled.div`
    display: grid;
    grid-template-columns: 1fr;
    grid-auto-rows: max-content;
    justify-items: center;
    row-gap: 0.35em;
`;

const List = styled.div`
    display: grid;

    grid-auto-rows: max-content;
`;

const addCategory = async (categoryName: string) => {
    const fs = firebase.firestore();

    const alreadyExsisted =
        (
            await fs
                .collection("posts")
                .where("category", "array-contains", categoryName)
                .limit(1)
                .get()
        ).size > 0;

    if (!alreadyExsisted) {
        fs.collection("posts").add({
            category: [categoryName],
            content: `新規カテゴリ"${categoryName}"が作成されました。`,
            contributor: "system",
            periodBegin: firebase.firestore.Timestamp.now(),
            periodEnd: null,
            title: "",
        });
    }
};

type Props = {
    posts: Post[];
    selected: string;
    setSelected: (idx: string) => void;
};

export const SideMenu = (props: Props) => {
    const [modalIsShown, setModalIsShown] = useState<boolean>(false);
    const [newCategoryName, setNewCategoryName] = useState<string>("");

    const categories = new Set<String>();
    for (const post of props.posts) {
        for (const category of post.category) {
            categories.add(category);
        }
    }

    return (
        <Base>
            <Modal
                isShown={modalIsShown}
                title="新規カテゴリを追加"
                onClose={() => setModalIsShown(false)}
            >
                <ModalBody>
                    <KeyValue>
                        カテゴリ名
                        <Input
                            value={newCategoryName}
                            onInput={(e) =>
                                setNewCategoryName(e.currentTarget.value)
                            }
                        />
                    </KeyValue>
                    <Btn
                        variant="Primary"
                        onClick={async () => {
                            if (newCategoryName.length > 0) {
                                await addCategory(newCategoryName);
                                setModalIsShown(false);
                            }
                        }}
                    >
                        追加
                    </Btn>
                </ModalBody>
            </Modal>
            <List>
                <MenuItem
                    selected={props.selected == ""}
                    onClick={() => props.setSelected("")}
                >
                    ホーム
                </MenuItem>

                <Fragment>
                    {Array.from(categories.entries()).map(([category]) => {
                        return (
                            <MenuItem
                                selected={category == props.selected}
                                onClick={() =>
                                    props.setSelected(category.toString())
                                }
                            >
                                {category}
                            </MenuItem>
                        );
                    })}
                </Fragment>

                <MenuButton
                    onClick={() => {
                        setModalIsShown(true);
                        setNewCategoryName("");
                    }}
                >
                    追加
                </MenuButton>
            </List>
        </Base>
    );
};
