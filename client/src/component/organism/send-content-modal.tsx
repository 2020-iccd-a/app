import React, { useState } from "react";
import { Modal } from "src/component/molecule/modal";
import styled from "styled-components";
import { Input } from "src/component/atom/input";
import { Textarea } from "src/component/atom/textarea";
import firebase from "firebase/app";
import "firebase/firestore";
import { Btn } from "src/component/atom/btn";
import { Option, Some, None } from "src/lib/option";

type Props = {
    isShown: boolean;
    onClose: () => void;
    fs: firebase.firestore.Firestore;
    category: string;
};

type Img = {
    url: string;
    type: string;
    data: Uint8Array;
};

const addContent = async (
    fs: firebase.firestore.Firestore,
    categoryName: string,
    title: string,
    content: string,
    img: Option<Img>
) => {
    fs.collection("posts").add({
        category: [categoryName],
        content: content,
        contributor: "system",
        periodBegin: firebase.firestore.Timestamp.now(),
        periodEnd: null,
        title: title,
        img: img
            .map((i) => ({
                type: i.type,
                data: firebase.firestore.Blob.fromUint8Array(i.data),
            }))
            .unwrap(),
    });
};

const ModalBody = styled.div`
    display: grid;
    grid-template-columns: 1fr;
    grid-auto-rows: max-content;
    justify-items: stretch;
    row-gap: 0.35em;
`;

const ModalFooter = styled.div`
    display: grid;
    grid-template-columns: 1fr max-content;
    grid-template-rows: 1fr;
`;

const ContentOptionContainer = styled.div`
    display: flex;
`;

const ContentOption = styled.div`
    display: grid;
    grid-template-rows: max-content 1fr;
    grid-template-columns: max-content;
`;

const OptionImg = styled.object`
    max-height: 4em;
    max-width: 100%;
`;

const SendBtn = styled(Btn)`
    padding-left: 2em;
    padding-right: 2em;
`;

export const SendContentModal = (props: Props) => {
    const [title, setTitle] = useState<string>("");
    const [content, setContent] = useState<string>("");
    const [img, setImg] = useState<Option<Img>>(new None());

    return (
        <Modal
            isShown={props.isShown}
            title="新規投稿"
            onClose={() => props.onClose()}
        >
            <ModalBody>
                <Input
                    value={title}
                    onInput={(e) => setTitle(e.currentTarget.value)}
                />
                <Textarea
                    value={content}
                    onInput={(e) => setContent(e.currentTarget.value)}
                />
                <ModalFooter>
                    <ContentOptionContainer>
                        <ContentOption>
                            <Btn
                                variant="Primary"
                                onClick={async () => {
                                    const input = document.createElement(
                                        "input"
                                    );
                                    input.type = "file";
                                    input.onchange = async (e) => {
                                        const files = (e.target as HTMLInputElement)
                                            .files;
                                        if (files) {
                                            const file = files[0];
                                            const url = URL.createObjectURL(
                                                file
                                            );
                                            const arrayBuffer = await file.arrayBuffer();
                                            setImg(
                                                new Some({
                                                    url: url,
                                                    type: file.type,
                                                    data: new Uint8Array(
                                                        arrayBuffer
                                                    ),
                                                })
                                            );
                                        }
                                    };
                                    input.click();
                                }}
                            >
                                画像を追加
                            </Btn>
                            { }
                            <OptionImg data={img.map((i) => i.url).or("")} />
                        </ContentOption>
                    </ContentOptionContainer>
                    <SendBtn
                        variant="Primary"
                        onClick={async () => {
                            await addContent(
                                props.fs,
                                props.category,
                                title,
                                content,
                                img
                            );
                            props.onClose();
                        }}
                    >
                        投稿
                    </SendBtn>
                </ModalFooter>
            </ModalBody>
        </Modal>
    );
};
