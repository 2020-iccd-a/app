import styled, { css } from "styled-components";
import { color } from "src/color";

export type Props = {
    selected: boolean;
};

const Base = styled.div`
    padding: 1.35em;
    border-bottom: 0.1em solid ${color.gray[5]};

    &:hover {
        cursor: pointer;
    }

    ${(props: Props) =>
        props.selected
            ? css`
                  background-color: ${color.gray[2]};
                  color: ${color.blue[5]};
              `
            : css`
                  background-color: ${color.gray[1]};
                  color: ${color.gray[9]};
              `}
`;

export const MenuItem = Base;
