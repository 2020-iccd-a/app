import styled, { css } from "styled-components";
import { color } from "src/color";
import React from "react";

export type Props = {
    onClick?: React.EventHandler<React.MouseEvent>;
};

const Base = styled.div`
    padding: 0.7em;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const Button = styled.button`
    outline: none;
    padding: 0.65em 2.35em;
    border-radius: 1.65em;
    background-color: ${color.blue[5]};
    color: ${color.gray[0]};
    border: none;
    box-shadow: none;

    &:hover {
        cursor: pointer;
    }
`;

export const MenuButton = (props: React.PropsWithChildren<Props>) => {
    return (
        <Base>
            <Button onClick={props.onClick}>{props.children}</Button>
        </Base>
    );
};
