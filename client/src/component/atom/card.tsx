import styled, { css } from "styled-components";
import React from "react";
import { color } from "src/color";

type Props = {
    heading: string;
};

const Base = styled.div`
    padding: 1.35em;
    background-color: ${color.gray[1]};
    color: ${color.gray[9]};
    box-shadow: 0 0 0.1em 0.07em ${color.gray[9]};
`;

const Heading = styled.h2`
    margin: 0;
`;

export const Card = (props: React.PropsWithChildren<Props>) => {
    return (
        <Base>
            <Heading>{props.heading}</Heading>
            {props.children}
        </Base>
    );
};
