import styled, { css } from "styled-components";

const Base = styled.div`
    display: grid;
    grid-template-columns: max-content 1fr;
    grid-auto-rows: max-content;
    row-gap: 0.35em;
    column-gap: 0.35em;
    align-items: center;
`;

export const KeyValue = Base;
