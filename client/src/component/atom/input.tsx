import styled, { css } from "styled-components";
import { color } from "src/color";

const Base = styled.input`
    outline: none;
    border: none;
    box-shadow: 0 0 0.07em 0.04em ${color.gray[9]} inset;
    line-height: 1.2;
    padding: 0.35em;
    border-radius: 0.35em;
    font-size: 1.2em;
`;

export const Input = Base;
