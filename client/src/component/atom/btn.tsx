import styled, { css } from "styled-components";
import { color } from "src/color";

export type Props = {
    variant: "Primary" | "Secondary";
};

const Base = styled.button`
    outline: none;
    padding: 0.35em 1.35em;
    border-radius: 0.2em;
    border: none;
    box-shadow: none;

    &:hover {
        cursor: pointer;
    }

    ${(props: Props) => {
        switch (props.variant) {
            case "Primary":
                return css`
                    background-color: ${color.blue[5]};
                    color: ${color.gray[0]};
                `;
            case "Secondary":
                return css`
                    background-color: ${color.gray[5]};
                    color: ${color.gray[0]};
                `;
        }
    }}
`;

export const Btn = Base;
