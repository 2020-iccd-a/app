import { Page, Heading, CardList } from "src/component/template/page";
import React, { useState } from "react";
import { Post } from "src/model/post";
import { Card } from "src/component/atom/card";
import { MenuButton } from "src/component/atom/menu-button";
import styled from "styled-components";
import firebase from "firebase/app";
import "firebase/firestore";
import { Env } from "src/model/env";
import { SendContentModal } from "src/component/organism/send-content-modal";

type Props = {
    posts: Post[];
    category: string;
    env: Env;
    fs: firebase.firestore.Firestore;
};

const ALine = styled.p`
    margin: 0;
`;

const OptionImg = styled.object`
    max-height: 20em;
`;

export const Category = (props: Props) => {
    const [modalIsShown, setModalIsShown] = useState<boolean>(false);

    return (
        <Page>
            <SendContentModal
                isShown={modalIsShown}
                onClose={() => setModalIsShown(false)}
                category={props.category}
                fs={props.fs}
            />
            <Heading>
                <h1>{props.category}</h1>
                {props.env == "Student" ? (
                    <></>
                ) : (
                        <MenuButton onClick={() => setModalIsShown(true)}>
                            投稿
                        </MenuButton>
                    )}
            </Heading>
            <CardList>
                {props.posts
                    .filter(
                        (post) =>
                            post.category.findIndex(
                                (category) => category == props.category
                            ) != -1
                    )
                    .map((post) => {
                        return (
                            <Card heading={post.title}>
                                {post.content.split("\n").map((aLine) => (
                                    <ALine>{aLine}</ALine>
                                ))}
                                {post.img
                                    .map((imgUrl) => (
                                        <OptionImg data={imgUrl} />
                                    ))
                                    .unwrap()}
                            </Card>
                        );
                    })}
            </CardList>
        </Page>
    );
};
