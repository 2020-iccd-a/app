import { Page, Heading, CardList } from "src/component/template/page";
import React from "react";
import { Post } from "src/model/post";
import { Card } from "src/component/atom/card";
import { Env } from "src/model/env";
import { MenuButton } from "src/component/atom/menu-button";
import styled from "styled-components";

type Props = {
    posts: Post[];
    env: Env;
    setEnv: (env: Env) => void;
};

const ALine = styled.p`
    margin: 0;
`;

const OptionImg = styled.object`
    max-height: 20em;
`;

export const Home = (props: Props) => {
    return (
        <Page>
            <Heading>
                <h1>ホーム（2020年12月17日リリース）</h1>
                {props.env == "Student" ? (
                    <MenuButton onClick={() => props.setEnv("Teacher")}>
                        教職員用に切り替える
                    </MenuButton>
                ) : (
                    <MenuButton onClick={() => props.setEnv("Student")}>
                        学生用に切り替える
                    </MenuButton>
                )}
            </Heading>
            <CardList>
                {props.posts.map((post) => {
                    return (
                        <Card heading={post.title}>
                            {post.content.split("\n").map((aLine) => (
                                <ALine>{aLine}</ALine>
                            ))}
                            {post.img
                                .map((imgUrl) => <OptionImg data={imgUrl} />)
                                .unwrap()}
                        </Card>
                    );
                })}
            </CardList>
        </Page>
    );
};
