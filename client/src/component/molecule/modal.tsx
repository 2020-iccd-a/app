import styled, { css } from "styled-components";
import { color } from "src/color";
import { MenuItem } from "src/component/atom/menu-item";
import { MenuButton } from "src/component/atom/menu-button";
import React from "react";

type Props = {
    title: string;
    isShown: boolean;
    onClose?: () => void;
};

const Background = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    justify-content: center;
    align-items: center;
    width: 100vw;
    height: 100vh;

    ${(props: { isShown: boolean }) =>
        props.isShown
            ? css`
                  display: flex;
              `
            : css`
                  display: none;
              `}
`;

const Base = styled.div`
    width: 40rem;
    max-width: 100vw;
    max-height: 90vh;

    box-shadow: 0 0 0.1em 0.07em ${color.gray[9]};
    background-color: ${color.gray[1]};

    padding: 0.35em;

    border-radius: 0.35em;
`;

const Header = styled.div`
    border-bottom: 0.1em solid ${color.gray[5]};
    display: grid;
    grid-template-columns: 1fr max-content;
    align-items: center;
    padding-bottom: 0.35em;
`;

const CloseButton = styled.button`
    outline: none;
    border: none;
    box-shadow: none;
    background-color: transparent;
    line-height: 1;
    padding: 0;
    font-size: 2em;

    &:hover {
        cursor: pointer;
    }
`;

const Body = styled.div`
    padding-top: 0.35em;
`;

export const Modal = (props: React.PropsWithChildren<Props>) => {
    return (
        <Background isShown={props.isShown}>
            <Base>
                <Header>
                    {props.title}
                    <CloseButton onClick={props.onClose}>×</CloseButton>
                </Header>
                <Body>{props.children}</Body>
            </Base>
        </Background>
    );
};
