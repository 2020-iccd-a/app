import React from "react";
import ReactDOM from "react-dom";
import { App } from "src/component/app";
import firebase from "firebase/app";

const initializeApp = () => {
    firebase.initializeApp({
        apiKey: "AIzaSyCkGUiI5XrM1Mnzs5C6yZKsyK8NKWoB2cQ",
        authDomain: "iccd-2020-team-a.firebaseapp.com",
        projectId: "iccd-2020-team-a",
    });

    ReactDOM.render(
        <App fs={firebase.firestore()}></App>,
        document.getElementById("app")
    );

    if ("serviceWorker" in navigator) {
        navigator.serviceWorker.register("/sw.js");
    }
};

window.addEventListener("load", initializeApp);
