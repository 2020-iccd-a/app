const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

const main = {
    mode: "production",
    entry: "./src/index.tsx",
    output: {
        path: path.join(__dirname, "./dist"),
        filename: "index.js",
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
            },
        ],
    },
    devServer: {
        contentBase: path.join(__dirname, "./dist"),
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx"],
        plugins: [new TsconfigPathsPlugin({ configFile: "./tsconfig.json" })],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "./index.html"),
            filename: "index.html",
        }),
    ],
};

const servieWorker = {
    mode: "production",
    entry: "./service_worker/main.ts",
    output: {
        path: path.join(__dirname, "./dist"),
        filename: "sw.js",
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
            },
        ],
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx"],
        plugins: [new TsconfigPathsPlugin({ configFile: "./tsconfig.json" })],
    },
};

module.exports = [main, servieWorker];
